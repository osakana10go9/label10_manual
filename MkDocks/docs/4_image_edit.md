画像の更新と追加
============================================================

__詳細マニュアル__ では、画像の数が多くなため、画像を機械的に管理できるよう
['画像リスト'シート](https://docs.google.com/spreadsheets/d/1GU03F2i96BPJNNmIup_QvYHfNOaXya3jn74SI6by23s/edit#gid=754151426) を使用します。
ここでは主にの詳細マニュアルの画像追加に関して書きます。

__かんたんマニュアル__ では画像の数が少ないので、別シートは使用しません。  
[操作が異なるため別に説明します](#_7)。  


___ ___ ___ ___ ___ ___ ___ ___ ___ ___


スプレッドシートへの画像の登録
------------------------
画像の新規登録は、該当ページの画像が並んでいる部分の最下部に行を追加し、情報を入力します。  

* サイト内で繰り返し使用される場合、[共通画像として登録](#i-3a)してください。
* 別ページで表示している画像を転用する場合、[転用画像として登録](#i-3b)してください。

画像登録の流れは以下の通りです。

1. [列追加](#i-1)
2. [カテゴリ、ページ情報入力](#i-2)
3. [画像の情報入力](#i-3)
4. [タグ出力確認](#i-4)

### [ i-1 ] 列追加
__同一ページの画像が並んでいる部分の最下部に行を挿入__ します。
途中に追加すると表示がくずれる可能性があります。

![画像リスト 行追加](./img/img_add.png "画像リスト 行追加")  

挿入した行に新規画像の情報を入力していきます。

### [ i-2 ] カテゴリ、ページ情報入力
カテゴリNO、カテゴリ名、カテゴリID、 ページNO、ページ名、ページID を上の行からコピーアンドペーストで入力します。  
初めて画像を表示するページの場合は、それぞれの項目を手動で入力します。

### [ i-3 ] 画像の情報入力
ページ内画像NO、画像タイトル、画像拡張子、画像タイプ を入力します。  

![画像リスト ID入力](./img/img_id.png "画像リスト ID入力")  

* __ページ内画像NO:__ 連番で該当する数字を入れます。
* __画像タイトル:__ 画像の中身がわかる名前をつけます。
* __画像タイプ:__ プルダウンから画像に合ったスタイルを選択します。選択しない場合、画像にスタイルは適用されません。くわしくは[__'本文記述ルール/設定'シート__](https://docs.google.com/spreadsheets/d/1GU03F2i96BPJNNmIup_QvYHfNOaXya3jn74SI6by23s/edit#gid=1245995930)の下のリストを参照してください。
* __画像拡張子:__ 画像フォルダに入れた画像の拡張子を入力します。誤った拡張子を入力した場合、画像が表示できません。

### [ i-3a ] 共通画像の情報入力
サイト内で繰り返し使用される画像は、共通画像としてして[__'共通画像リスト'シート__](https://docs.google.com/spreadsheets/d/1GU03F2i96BPJNNmIup_QvYHfNOaXya3jn74SI6by23s/edit#gid=2015237711)
に登録されています。共通画像は、このシート内の画像表記を参照して画像を登録します。

* [i-1, i-2 の作業](#i-1) を行なったら、ページ内画像NO のみを連番で入力します。
* '共通画像リスト'シート を開き、表示したい画像の __画像タイトル__ をコピーします。 
* コピーした画像タイトルを '画像リスト'シート の画像タイトル にペーストします。  
("共通:フォント" など)
* __行が灰色__ になって __markdown表記の列に画像表記が表示される__ ことを確認します。

![画像リスト 共通画像](./img/img_common.png "画像リスト 共通画像")  


### [ i-3b ] 転用画像の情報入力
他ページで登録済みの画像を表示する場合は、転用画像として ’画像リスト’シート 内に既に登録されている画像表記を参照して画像を登録します。

* [i-1, i-2 の作業](#i-1) を行なったら、ページ内画像NO のみを連番で入力します。
* ’画像リスト’シート 内から、表示したい画像の __画像タイトル__ をコピーします。 
* コピーした画像タイトルを '画像リスト'シート の画像タイトル にペーストします。  
("転用:トップ画面" など)
* ペーストした画像タイトルの先頭に、”” を追加します。
* __行が緑色__ になって __markdown表記の列に画像表記が表示される__ ことを確認します。

![画像リスト 転用画像](./img/img_refer.png "画像リスト 転用画像")  


### [ i-4 ] タグ出力確認
必要な情報が入力されると、[markdown 表記] の列に下記のような画像表記が自動的に合成されます。
__合成されていない場合、登録情報に問題があります__。  

    ![全体画面 通常文字フォント変更前](./images/2/2_01-operation_tips_06.png "通常文字フォント変更前")


___ ___ ___ ___ ___ ___ ___ ___ ___ ___


画像の追加
------------------------
スプレッドシートに画像が登録できたら、実際にページ上に表示します。

* 'ページ原稿'シートの確認
* 画像に名前をつけてフォルダに配置
* csvのダウンロード、ビルド、確認

### 'ページ原稿'シートの編集、確認

表示したいページの __section_X_img__ セルで、
既存画像の末尾に `&" 改行　"&` を追加してから、__新しい画像のVLOOKUPを記述__ します。

表示したいページの __section_X_txt__ セル内の画像を表示したい箇所に、
__<imgN コメント> を追加__ します。N は section_X_img 内に記述されている行数です。

### 画像に名前をつけてフォルダに配置
    label_yasan/
      └ htdocs/
          └ manual/ (詳細マニュアル)
              └ images/
                  ├ 1/
                  ├ 2/
                  ├ 3/
                  ├ 4/
                  │   └ common/
                  ├ 5/
                  ├ 6/
                  └ 7/




### csvのダウンロード、ビルド、確認
[__ページ更新の p-2, p-3, p-4__](./3_page_edit.html#p-2-csv) と同じ手順を行います。

確認の際には追加した画像の表示をチェックします。

* 正しい画像が表示れている。
* 画像タイプ で指定した画像が表示されている。

___ ___ ___ ___ ___ ___ ___ ___ ___ ___


画像の削除
------------------------
['ページ原稿'シート](https://docs.google.com/spreadsheets/d/1GU03F2i96BPJNNmIup_QvYHfNOaXya3jn74SI6by23s/edit#gid=0)
内の画像使用セルから、表示指定を削除します。section_X_txt、section_X_img 両方から削除します。

* section_X_txt 内から該当の `<imgX テキスト>` を削除します。
* section_X_img から該当の VLOOKUP を削除します。

__'画像リスト'シート のデータは削除しない__ でください。  
'画像リスト'シート のデータを削除すると、それ以降の画像表示が失敗する可能性があります。


___ ___ ___ ___ ___ ___ ___ ___ ___ ___


かんたんマニュアルの画像の更新
------------------------
    label_yasan/
      └ htdocs/
          └ manual_easy/ (かんたんマニュアル)
              └ images/

### 画像の配置と保存
上記の階層に
`manual_[ページID]_sct_[セクションNO]_[画像NO].[画像の種類]` のようなファイル名をつけて画像を保存します。
セクションNOは 01、02、のように二桁で表示します。

ページIDが basic、セクションNOが 01、画像NOが 1、画像の種類が png の場合
`manual_basic_sct_01_1.png` というファイル名で保存します。

### 'ページ原稿'シート の更新
[かんたんマニュアル 'ページ原稿'シート ](https://docs.google.com/spreadsheets/d/1iQ2dAc6gIWcpxue3XN2Y81yhv5qpsds4QsFx0q619fE/edit#gid=0)を開き、
追加した画像のmarkdown記述を block_X_img に直接書きます。  

    ![画像01_1](./images/manual_basic_sct_01_1.png "画像01_1")

この時、__block_X_img に登録できる画像は一枚まで__ です。  
これにより、かんたんマニュアル 'ページ原稿'シート の block_X_img 内は常に一行です。

2枚ある場合は、表示したい場所に新たに section_X_name、section_X_txt、section_X_img のセットを挿入してください。
[__ページ追加の説明__ で記した注意点](./3_page_edit.html#p-1a)を確認しながら挿入してください。


### csvのダウンロード、ビルド、確認
[__ページ更新の p-2, p-3, p-4__](./3_page_edit.html#p-2-csv) と同じ手順を行います。

確認の際には追加した画像が正しく表示できているかチェックします。


