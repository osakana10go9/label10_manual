マニュアルの追加
============================================================

ここでは詳細マニュアルの複製から新しいマニュアルを追加する方法について書きます。

まず、新しい __スプレッドシート、ejsテンプレート、gulpのタスク__ を用意し、新しいマニュアルがビルドができる状態にします。
その後で、必要に応じてスプレッドシートとejsを変更します。

複製／追加の流れは以下の通りです。

1. スプレッドシートの複製
1. ejsの複製
1. タスクの複製
1. ビルドのテスト
1. 調整
1. ビルド


___ ___ ___ ___ ___ ___ ___ ___ ___ ___


詳細マニュアルの複製
-------------------------

### [ m-1 ] スプレッドシートの複製

[詳細マニュアル 'ページ原稿'シート ](https://docs.google.com/spreadsheets/d/1GU03F2i96BPJNNmIup_QvYHfNOaXya3jn74SI6by23s/edit#gid=0)
を開いて、スプレッドシート内メニューの __ファイル > コピーを作成...__ を選択し、シートを複製します。  


![シートの複製](./img/copy_sheet.png "シートの複製")  

複製したシート名の、末尾の "のコピー" を削除し ”詳細マニュアル” の部分を新しいマニュアル名に変更します。  

複製したシートから [__ページ更新の p-2__](./3_page_edit.html#p-2-csv) の手順で、
csvをダウンロードし `label_yasan/ejs/src/csv` に配置します。

    label_yasan/
      └ ejs/
          └ src/
              └ csv/
                  ├ ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv
                  └ ラベル屋さん10 [新しいマニュアル名を入力] ページデータ - ページ原稿.csv

___ ___ ___ ___ ___ ___ ___ ___ ___ ___

### [ m-2 ] ejsの複製

`label_yasan/ejs/src/ejs/template_manual.ejs ` を複製し、新しいマニュアルに合った名前に変更します。 (template_mbapp.ejs 等)

    label_yasan/
      └ ejs/
          └ src/
              └ ejs/
                  ├ template_manual.ejs
                  └ template_mbapp.ejs (新しいマニュアル用esjファイル例)

新規に作成したejsを開き、manual_type を新しいマニュアル用に書き換えます。

    json_data["manual_type"] = "MANUAL";
    ↓
    json_data["manual_type"] = "MBAPP";



___ ___ ___ ___ ___ ___ ___ ___ ___ ___

### [ m-3 ] タスクの複製

`label_yasan/ejs/tasks/manual.js` を複製し、新しいマニュアルに合った名前を末尾に追加します。(manual_mbapp.js 等)

    label_yasan/
      └ ejs/
          ├ gulpfile.js
          └ tasks/
              ├ manual.js
              └ manual_mbapp.js (新しいマニュアル用jsファイル例)

複製したjsのタスク内の 詳細マニュアル用の記述 を、新しいマニュアル用の記述 に変更します。

#### m-3-1 タスク名の変更
複製したファイル内のタスク名 __csv_pre__、__csv_to_json__、__ejs_manual__、新しいマニュアルに合った短い文字を末尾に追加します。  
(csv_pre_m、csv_to_json_m、ejs_manual_m 等)

    gulp.task('csv_pre', function(callback) {
    ↓
    gulp.task('csv_pre_m', function(callback) {


#### m-3-2 ロードするcsvとejsの変更
読み込むcsvをダウンロードしたcsvの名前に変更します。  
"詳細マニュアル" の部分を、新規に作成した新しいマニュアル名に変更します。

	var FILE = path.join(__dirname, '../src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv');
    ↓
	var FILE = path.join(__dirname, '../src/csv/ラベル屋さん10 [新しいマニュアル名を入力] ページデータ - ページ原稿.csv');

中間ファイルのcsvとjsonの指定を、新規に作成した新しいマニュアル名に変更します。


csv

	path: './src/csv/pages_.csv'
    ↓
	path: './src/csv/pages_mbapp.csv'

	-----

	var transposed_csv = '../src/csv/pages.csv';
    ↓
	var transposed_csv = '../src/csv/pages_mbapp.csv';

	-----

    gulp.src( "./src/csv/pages.csv" )
    ↓
    gulp.src( "./src/csv/pages_bmapp.csv" )

json

    var jsonFile = require("../src/json/pages.json");
    ↓
    var jsonFile = require("../src/json/pages_mbapp.json");

ejsの指定を、新規に作成した新しいマニュアル名に変更します。

    gulp.src("./src/ejs/template_manual.ejs")
    ↓
    gulp.src("./src/ejs/template_mbapp.ejs") (新しいマニュアル用ejsファイル例)



#### m-3-3 生成したHTMLの出力先の変更
読み込むcsvをダウンロードしたcsvの名前に変更します。  

    .pipe(gulp.dest("../htdocs/manual/")); // 出力先指定
    ↓
    .pipe(gulp.dest("../htdocs/manual_mbapp/")); // 出力先指定 (新しいマニュアル用 出力先例)


___ ___ ___ ___ ___ ___ ___ ___ ___ ___

### [ m-4 ] ビルドとのテスト

[ページ更新の p-3、p-4](./3_page_edit.html#p-3-html) の手順で、m-3-1で変名した3つのタスクを実行します。  
先程の例では以下のようになります。

    cd クローンした階層/label_yasan/ejs/
    npx gulp csv_pre_m
    npx gulp csv_to_json_m
    npx gulp ejs_manual_m

`label_yasan/htdocs/manual_mbapp/ (新しいマニュアル用 出力先例)` に詳細マニュアルと同じものが出力されていれば成功です。

どれかのhtmlのソースを開いて、以下のようにbodyタグのクラスに 新しいマニュアル用の manual_type が表示されていることを確認します。
(この場合は __MBAPP__ )

    <body id="firstly_about" class="MBAPP cat_firstly page__">

新しいマニュアルの出力先に、`label_yasan/htdocs/manual/` 内からcssなどの以下のフォルダをコピーします。

    label_yasan/
      └ htdocs/
          └ manual/
              ├ common
              ├ stylesheets
              ├ images
              ├ js
              └ fonts

先ほど表示したページに、スタイルがされた表示がされたらOKです。

___ ___ ___ ___ ___ ___ ___ ___ ___ ___

新規マニュアル用の調整
-------------------------

### [ m-5 ] ejsとスプレッドシートの調整

HTMLの構造を変えるには、新規ページ用のejsとの更新と　`_header.ejs` `_footer.ejs` のHTMLを更新します。

    label_yasan/
      └ ejs/
          └ src/
              └ ejs/
                  ├ _header.ejs (共通ヘッダ)
                  └ _footer.ejs (共通フッタ)

#### メニュー分岐の追加
メニューは新規ページ用ejsの外に記述されているので `_header.ejs` を開き、新しいマニュアル専用にメニューを追加します。  

以下のマニュアル分岐箇所を複製してから新しいマニュアル用にメニューを書き換えます。

    } else if( page_settings.manual_type === "MANUAL" ){
    // 詳細マニュアル メニュー
    ...

必要に応じて、それ以外の分岐箇所にも新しいマニュアル用の分岐を追加して、表示を追加します。


#### ページの増減
ページはスプレッドシートで管理されていて、
1列目は項目の日本語名、2列目がejsテンプレートで使う変数名になっています。  

3列目以降の1列1列が、それぞれのページになっていて、  
列を追加削除することでページの追加削除ができます。  

##### ページの追加
[__ページ追加 p'-1 〜__](3_page_edit.html#_3) の手順で行ってください。

##### ページの削除
スプレッドシートの列を削除し、esjのメニュー部分から該当ページを削除します。


#### 表示データの追加、変更

スプレッドシート




画像リスト


詳細マニュアルと異なるものにする場合は、メニューの設定、スプレッドシートとgulpタスクの


___ ___ ___ ___ ___ ___ ___ ___ ___ ___

### [ m-6 ] ビルド
[__ページ更新の p-2, p-3, p-4__](./3_page_edit.html#p-2-csv) と同じ手順を行います。

確認の際には以下の項目をチェックします。

* [PC] 新規に追加したページのリンクがメニューにある。
* [PC] 新規ページを開いた時にメニューのリンクがハイライトされている。
* [PC] 新規ページのコンテンツが正しく表示されている。
* [モバイル] 新規に追加したページのリンクがメニューにある。
* [モバイル] 新規ページのコンテンツが正しく表示されている。


