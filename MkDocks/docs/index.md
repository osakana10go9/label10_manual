ラベル屋さん10 かんたん/詳細マニュアル 出力手順
============================================================

gitからクローンしたフォルダの内容は以下の通りです。

    label_yasan/
      ├ node_modules/ (生成用モジュール)
      ├ ejs/ (生成時に使うタスク、テンプレート、データ)
      ├ htdocs/ (生成したhtmlと画像cssなど表示用ファイル)
      ├ README/ (このドキュメント)
      └ MkDocks/ (このドキュメントの元ファイル)
  
___ ___ ___ ___ ___ ___ ___ ___ ___ ___

ビルドの概要/流れ
--------------------

ラベル屋さん10 かんたん/詳細マニュアルのHTMLファイルは以下の方式で出力しています。

* __googleスプレッドシート__ にページ原稿を書き、それをcsvとして保存したファイルを使って全ページを生成します。
* __ejs__ のテンプレートにcsvの原稿を流し込み、中身の異なるhtmlを生成します。
* ejsによる変換処理の実行はTerminalなどから __gulp__ というタスクランナーで行います。
* ejs と gulp は __Node.js__ をインストールした環境で動作します。

### 具体的な操作

1. __環境構築:__  
作業フォルダを作成し、Node.jsをインストールします。  
 　↓
2. __ejsテンプレートの作成/編集:__  
csvのデータを流し込むHTMLのテンプレートを編集します。  
 　↓
3. __画像等のファイルの用意:__  
表示に用いる 画像、css、js、フォント などを用意します。  
 　↓
4. __ページ情報の登録:__  
指定のスプレッドシートの列に、各ページのコンテンツを記述し原稿を作成します  
 　↓
5. __csvダウンロード/ビルド:__  
ページ原稿をcsvでダウンロードし、gulpのタスクを実行しHTMLを生成します。  


初回のみ1からの作業になりますが、  
環境が用意できた後は、4か2からの作業になります。

__文言と画像の修正、sectionの増減:__   
→ サイトの構造を変えない修正は、__4__ から作業を行います。

__ページの追加、サイドメニューの更新:__   
→ サイトの構造に関わる修正は、__2__ から作業を行います。

___ ___ ___ ___ ___ ___ ___ ___ ___ ___

ビルドの環境構築
--------------------



### Node.js のインストール
アプリケーションを実行するプラットフォームをインストールします。  
[> 各OS別のインストール方法まとめ (外部サイト)](https://www.sejuku.net/blog/72545)

npm のバージョンが 5.2 以上でなければ、更新が必要です。  

    npm update npm
_※ コードの表記はTerminalなどからコマンドラインで入力する内容です_

Node.js のインストール以降は __基本的にはgitでクローンした環境を使用してください。__  
必要なパッケージがビルド時に自動でインストールされます。

もし何かの際に、必要な環境をゼロからインストールする場合は以下の設定が必要です。  

### Node.js プロジェクトの作成
label_yasan/ フォルダを作成、移動してnpmで初期化(プロジェクトを作成)します。

    mkdir /Users/ユーザー名/任意の階層/label_yasan
    cd /Users/ユーザー名/任意の階層/label_yasan
    npm init -y

### gulp のインストール
変換処理を実行するNode.jsのパッケージです。  
npmでインストールします。
[> gulp4のインストール](https://qiita.com/tonkotsuboy_com/items/9ab83fe0f25cf0b010f3)

    npm install gulp -D

### Node.jsモジュール	のインストール
gulpでの処理に必要なモジュールです。  
gulpと同様に一個ずつnpmでインストールします。

    npm i -D require-dir
    npm i -D path
    npm i -D del
    npm i -D fs
    npm i -D through2
    npm i -D marked
    npm i -D csv
    npm i -D csv-writer
    npm i -D gulp-convert
    npm i -D gulp-rename
    npm i -D gulp-ejs


<!--

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.





    ┏━━━━━━━━━━━━━━━━━━━━━━━━━
    ┃  htdocs 
    ┃　┏━━━━━━━━━━━━━━━━━━━━━━━━━ 
    ┃　┃  manual // 詳細マニュアル html      
    ┃　┃　
    ┃　┃　─ index.html
    ┃　┃　─ 1_02-firstly_caution.html
    ┃　┃　─ 1_03-firstly_specification.html
    ┃　┃　─ ...
    ┃　┃　
    ┃　┃　┌─────────────────┐
    ┃　┃　  stylesheets
    ┃　┃　┌─────────────────┐
    ┃　┃　  images
    ┃　┃　┌─────────────────┐
    ┃　┃　  js
    ┃　┃　┌─────────────────┐
    ┃　┃　  fonts
    ┃　┃　┌─────────────────┐
    ┃　┃　  common
    ┃　
    ┃　┏━━━━━━━━━━━━━━━━━━━━━━━━━ 
    ┃　┃　manual_easy // かんたんマニュアル html
    ┃　┃　
    ┃　┃　─ manual_easy_pouring.html
    ┃　┃　─ manual_easy_basic.html
    ┃　┃　
    ┃　┃　┌─────────────────┐
    ┃　┃　  stylesheets
    ┃　┃　┌─────────────────┐
    ┃　┃　  images
    ┃
    ┃　┌─────────────────┐
    ┃　  support // かんたんマニュアル 確認用ファイル
    ┃　┌─────────────────┐
    ┃　  common  // かんたんマニュアル 確認用ファイル
    ┃　┌─────────────────┐
    ┃　  js      // かんたんマニュアル 確認用ファイル
    

    


cssの編集
============================================================
元サイトのcssを上書き


かんたんマニュアル
    .css


詳細マニュアル
sass
compass でビルド
    ベンダープレフィックスをimportに依存   

モバイルファースト

印刷

-->