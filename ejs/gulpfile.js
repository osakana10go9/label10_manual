// require:
// ------------------------------------------------------------
const gulp       = require('gulp');
const requireDir = require('require-dir');

requireDir('./tasks', {recurse: true});

// task: 
// ------------------------------------------------------------
// npx gulp [task] で実行 | gulp.task('[task]', function() {});


// watch
// ==================================================
gulp.task('watch', function(cb_done) {
	// var path = './src/csv/**/*.csv';
	var path = "./src/csv/pages_raw.csv"
    // gulp.watch('./sass/**/*.scss', gulp.task('sass'));
	// gulp.watch( path, ["changed"] );
	gulp.watch( path, gulp.task('build_manual') );
	
	// コールバック
	cb_done();
});

// watch test
// ==================================================
gulp.task('watch_test', function(cb_done) {
	var path = './src/csv/**/*.csv';
	// var path = "./csv/pages_raw.csv"

	gulp.watch(path, function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		/* console.log(e.type)  added, changed or deleted のどれかが発生するはず */
		/* console.log(e.path)  何らかの変更があった対象ファイルの絶対パス */
		}
	);
	
	// コールバック
	cb_done();
});




