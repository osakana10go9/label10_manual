// require:
// ------------------------------------------------------------
const gulp       = require('gulp');
const path       = require('path');
const del        = require('del');
const fs         = require('fs');
const through    = require('through2');
// const zero_pad = require('zero-fill');

const compile_md      = require('marked');
const csv_pkg         = require('csv');
const createCsvWriter = require('csv-writer').createArrayCsvWriter; 
const file_convert    = require('gulp-convert');
const rename          = require('gulp-rename');
const process_ejs     = require('gulp-ejs');


// csv前処理: 転置、整形 
// ==================================================
gulp.task('csv_pre', function(callback) {

	var transposed_csv = '../src/csv/pages.csv';
	del( transposed_csv );//.then(console.log('pages.csv (transposed) was deleted'));
	var FILE = path.join(__dirname, '../src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv');

	// CSV parse　設定
	const parse_param = {
		delimiter: ',', 
		quote: '"', 
		// 	columns: null, // 1行目をフィールド名に
		skip_empty_line: false, 
		trim: false // 両端のスペースを削除
	};
	// csv 保存 設定 
	const csvWriter = createCsvWriter({  
		path: './src/csv/pages.csv'// , header: ['ヘッダに','見出し行を','挿入する場合','ここに定義']
	});

	// ■ csvを配列にperse/加工して保存
	const preprocessor = csv_pkg.parse(parse_param, function(err, data_csv) {

		// perseした配列を転置
		var data_transposed = data_csv[0].map((_, c) => data_csv.map(r => r[c]));

		// フィールドラベル日本語名を削除
		data_transposed.shift();

		// csvに保存
		csvWriter  
		  .writeRecords( data_transposed );
		  // .then(()=> console.log('pages.csv (transposed) was written successfully'));
	});

	// 実行
	fs.createReadStream(FILE).pipe(preprocessor);
 //    gulp.src( "./src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv" )
	// .pipe(preprocessor);

	// コールバック
    callback(); 
});



// csv > json 変換 
// ==================================================
gulp.task('csv_to_json', function(callback) {

	// var pages_json = '../src/json/pages.json';
	// 	fs.writeFileSync(pages_json , "");

	// if ( fs.statSync(pages_json) ) {
	// }else{
	// 	fs.writeFile(pages_json , "");
	// 	console.log('pages.json was generated');
	// }

	// del(['../src/json/pages.json']);

	// ページ原稿csvをjsonに変換
    gulp.src( "./src/csv/pages.csv" )
    // gulp.src("./src/csv/pages.csv", { allowEmpty: true })
	.pipe( file_convert( {from: 'csv', to: 'json'} ) ) // 1行あたり1オブジェクトのjsonに変換
    .pipe( gulp.dest("./src/json") );

	// コールバック
    callback(); 
});


// ejs 詳細マニュアル
// ==================================================
gulp.task('ejs_manual', function(callback){

	// ！オプションでマークダウン.md ファイルを個別のページごとに吐き出すようにできないか
	// HTMLなしの別テンプレート ＋ エンコードしなければいい
	// [できたらやる] メインサイドメニューの自動更新
		// ejsで使用するテンプレートを更新
			// カテゴリがなければ ul　追加
			// ul内にli追加

	var domain_url = "http://www.labelyasan.com/";

	// ページ原稿jsonを読み込み
    var jsonFile = require("../src/json/pages.json");

    // ページ毎に設定、出力	
    jsonFile.forEach(function (pageData, i) {	
		var section_txt = [];
		var page_no_00 = ('00'+pageData.page_no).slice(-2);
		var page_filename = pageData.cat_no+'_'+page_no_00+'-'+pageData.cat_id+'_'+pageData.page_id+'.html'; 

		if( pageData.cat_no == 1 && pageData.page_no == 1 ){
			var page_filename = 'index.html'; 
		}

		// markdownのコンパイルと、画像データの埋め込み		
		for(i=0; i<=9; i++){
			var txt_data = eval('pageData.section_'+i+'_txt');
			var img_data = eval('pageData.section_'+i+'_img');

			// 画像データが存在すれば<imgX>の形を画像に置換
			if ( img_data != '' ) {
				// 改行ごとで画像にパースして配列化
				var images = img_data.split('\n'); 

				images.forEach(function(img, i){
					var img_no = i+1;
					var reg = new RegExp("<img"+img_no+"[^>]*>"); // <imgX>内にコメントを許容
					// txt_data内の<imgX>を置換
					txt_data = txt_data.replace(reg, img);
				});
			}

			// markdownをコンパイルして保存
			section_txt[i] = compile_md(txt_data);
		}

	    gulp.src("./src/ejs/template_manual.ejs")
           .pipe(process_ejs({
	           	// テンプレートファイルに渡すjsonデータ
                jsonData:{ 
					"cat_name" : pageData.cat_name,
					"cat_no"   : pageData.cat_no,
					"cat_id"   : pageData.cat_id,
					"page_name"        : pageData.page_name,
					"page_filename"    : page_filename, 
					"page_no"          : page_no_00,
					"page_id"          : pageData.page_id,
					"page_description" : pageData.page_description,

					// 可能なら後でオブジェクト化してLOOPで表示
					"section_name_0": pageData.section_0_name,
					"section_name_1": pageData.section_1_name,
					"section_name_2": pageData.section_2_name,
					"section_name_3": pageData.section_3_name,
					"section_name_4": pageData.section_4_name,
					"section_name_5": pageData.section_5_name,
					"section_name_6": pageData.section_6_name,
					"section_name_7": pageData.section_7_name,
					"section_name_8": pageData.section_8_name,
					"section_name_9": pageData.section_9_name,
					"section_txt_0" : section_txt[0],
					"section_txt_1" : section_txt[1],
					"section_txt_2" : section_txt[2],
					"section_txt_3" : section_txt[3],
					"section_txt_4" : section_txt[4],
					"section_txt_5" : section_txt[5],
					"section_txt_6" : section_txt[6],
					"section_txt_7" : section_txt[7],
					"section_txt_8" : section_txt[8],
					"section_txt_9" : section_txt[9],

					// 可能なら後でオブジェクト化してLOOPで表示
					/*
					<% jsonData.section.forEach(function (sectionData, i) { if( sectionData.name != '' ){ %> 
					<li class=""><a href="#section_<%= i %>"><%= sectionData.name %></a></li>
					<% }}); %>
					"section":[
						{
							"section_name_X": pageData.section_X_name,
							"section_txt_X" : pageData.section_X_txt,
							"section_sub_X" : pageData.section_X_sub,
						},
					*/

					"domain_url" : domain_url
                }
            }))
		    .pipe(rename(page_filename)) // ファイル名+拡張子指定
            // .pipe(gulp.dest("./dist/")); // 出力先指定
            .pipe(gulp.dest("../htdocs/manual/")); // 出力先指定
    });

	// コールバック
    callback(); 
});


// build manual
// ==================================================
gulp.task('build_manual',
	gulp.series(
	'csv_pre', 
	'csv_to_json',
	'ejs_manual', 
	function(callback){
	// コールバック
	callback();
}));


// csv前処理: 転置、整形 
// ==================================================
gulp.task('csv_pre2', function(callback) {


	var transposed_csv = '../src/csv/pages.csv';
	del([transposed_csv]);

	// var FILE = path.join(__dirname, '../src/csv/pages_raw.csv');

	// 転置関数
	function transpose(a) {
		return a[0].map(function (_, c) {return a.map(function (r) {return r[c]; }); });
	  // or in more modern dialect
	  // return array[0].map((_, c) => array.map(r => r[c]));
	}
	// const transpose = a => a[0].map((_, c) => a.map(r => r[c]));

	// ■ csvを配列にperseして、転置してから保存
	// const parse_param = {
	// 	trim: false // 両端のスペースを削除
	// };
	// const preprocessor = csv_pkg.parse( parse_param, function(err, data_csv) {
	// 	// perseした配列を転置
	// 	const data_transposed = transpose(data_csv);
	// 	// フィールドラベル日本語名を削除
	// 	data_transposed.shift();
	//   	// csv 保存設定 
	// 	// const csvWriter = createCsvWriter({  
	// 	// 	path: '../src/csv/pages.csv'// ,
	// 	// 	// header: ['ヘッダに','見出しの行を','挿入する場合','ここで定義']
	// 	// });
	// 	// csvに保存
	// 	// csvWriter  
	// 	//   .writeRecords( data_transposed )
	// 	//   .then(()=> console.log('pages.csv (transposed) was written successfully'));
	// });

	// 実行
	// fs.createReadStream(FILE).pipe(preprocessor);
	// const transpose = a => a[0].map((_, c) => a.map(r => r[c]));

    gulp.src("./src/csv/pages_raw.csv", { allowEmpty: true })
		// .pipe(file_convert({from: 'csv', to: 'json'})) // 1行あたり1オブジェクトのjsonに変換
		// .pipe(preprocessor)
	    .pipe( through.obj(function (data_csv, enc, callback) {
	      //このファンクションで独自にファイルの内容を変更できます

	      //拡張子を取得（ハイライトする言語タイプは拡張子に合わせる）
	      // var type = path.extname(file.path).replace('.', '');

	      // file.contents = "11","12","13","14","15"\n"21","22","23","24","25";
	      //new Buffer()をしないとエラーになる
	      data_csv.contents = new Buffer(

			// function transpose(a) {
			// 	return a[0].map(function (_, c) {return a.map(function (r) {return r[c]; }); });
			//   // or in more modern dialect
			//   // return array[0].map((_, c) => array.map(r => r[c]));
			// }
	  //     	// hljs.highlight(type, file.contents.toString()).value 
   //    		const parse_param = {
			// 	trim: false // 両端のスペースを削除
			// };
   //    		const preprocessor = csv_pkg.parse( parse_param, function(err, data_csv) {
			// 	// perseした配列を転置
			// 	const data_transposed = transpose(data_csv);
			// 	// フィールドラベル日本語名を削除
			// 	data_transposed.shift();
			// }
	      );

	      callback(null, data_csv);
	    }))

	    .pipe( rename("pages.csv")) // ファイル名+拡張子指定
        .pipe( gulp.dest("./src/csv/"));


	// コールバック
    callback(); 
});


// csv前処理: 転置、整形 
// ==================================================
gulp.task('csv_pre3', function(callback) {

	// var transposed_csv = '../src/csv/pages.csv';
	// del( transposed_csv );//.then(console.log('pages.csv (transposed) was deleted'));
	// var FILE = path.join(__dirname, '../src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv');

	// CSV parse　設定
	// const parse_param = {
	// 	delimiter: ',', 
	// 	quote: '"', 
	// 	// 	columns: null, // 1行目をフィールド名に
	// 	skip_empty_line: false, 
	// 	trim: false // 両端のスペースを削除
	// };
	// csv 保存 設定 
	// const csvWriter = createCsvWriter({  
	// 	path: './src/csv/pages.csv'// , header: ['ヘッダに','見出し行を','挿入する場合','ここに定義']
	// });


	// ■ csvを配列にperse/加工して保存
	// const preprocessor = csv_pkg.parse(parse_param, function(err, data_csv) {

	// 	// perseした配列を転置
	// 	var data_transposed = data_csv[0].map((_, c) => data_csv.map(r => r[c]));

	// 	// フィールドラベル日本語名を削除
	// 	data_transposed.shift();

	// 	// csvに保存
	// 	csvWriter  
	// 	  .writeRecords( data_transposed )
	// 	  .then(()=> console.log('pages.csv (transposed) was written successfully'));
	// });

	// 実行
	// fs.createReadStream(FILE).pipe(preprocessor);
 //    gulp.src( "./src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv" )
	// .pipe(preprocessor);


	const csv_parser = csv_pkg.parse({delimiter: ','}, function (data_csv) {
		console.log( data_csv );
		return data_csv;
	});

	const csv_transformer = csv_pkg.transform(function (err, data_csv) {
		// perseした配列を転置
		var data_transformed = data_csv[0].map((_, c) => data_csv.map(r => r[c]));
		// フィールドラベル日本語名を削除
		data_transformed.shift();
		
		return data_transformed;
	});

	// 実行
	// fs.createReadStream(FILE).pipe(preprocessor);
    gulp.src( "./src/csv/ラベル屋さん10 詳細マニュアル ページデータ - ページ原稿.csv" )
	.pipe(csv_parser)
	.pipe(csv_transformer)
	// .pipe( file_convert( {from: 'csv', to: 'json'} ) ) // 1行あたり1オブジェクトのjsonに変換
	// .pipe( gulp.dest("./src/json") );
	.pipe(rename('pages4.csv')) // ファイル名+拡張子指定
	.pipe( gulp.dest("./src/csv") );




	// コールバック
    callback(); 
});




