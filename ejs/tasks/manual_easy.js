// require:
// ------------------------------------------------------------
const gulp       = require('gulp');
const path       = require('path');
const del        = require('del');
const fs         = require('fs');
const through    = require('through2');
// const zero_pad = require('zero-fill');

const compile_md      = require('marked');
const csv_pkg         = require('csv');
const createCsvWriter = require('csv-writer').createArrayCsvWriter; 
const file_convert    = require('gulp-convert');
const rename          = require('gulp-rename');
const process_ejs     = require('gulp-ejs');


// csv前処理: 転置、整形 
// ==================================================
gulp.task('csv_pre_e', function(callback) {

	var transposed_csv = '../src/csv/pages_easy.csv';
	del( transposed_csv );//.then(console.log('pages_easy.csv (transposed) was deleted'));
	var FILE = path.join(__dirname, '../src/csv/ラベル屋さん10 かんたんマニュアル ページデータ - ページ原稿.csv');
	
	// CSV parse　設定
	const parse_param = {
		delimiter: ',', 
		quote: '"', 
		// 	columns: null, // 1行目をフィールド名に
		skip_empty_line: false, 
		trim: false // 両端のスペースを削除
	};
  	// csv 保存設定 
	const csvWriter = createCsvWriter({  
		path: './src/csv/pages_easy.csv'// , // header: ['ヘッダに','見出しの行を','挿入する場合','ここで定義']
	});

	// ■ csvを配列にperse/加工して保存
	const preprocessor = csv_pkg.parse(parse_param, function(err, data_csv) {

		// perseした配列を転置
		// const data_transposed = transpose(data_csv);
		var data_transposed = data_csv[0].map((_, c) => data_csv.map(r => r[c]));

		// フィールドラベル日本語名を削除
		data_transposed.shift();

		// csvに保存
		csvWriter  
		  .writeRecords( data_transposed )
		  .then(()=> console.log('CSV file (transposed) was written successfully'));
	});

	// 実行
	fs.createReadStream(FILE).pipe(preprocessor);

	// コールバック
    callback(); 
});


// csv > json 変換 
// ==================================================
gulp.task('csv_to_json_e', function(callback) {

	// ページ原稿csvをjsonに変換
    gulp.src("./src/csv/pages_easy.csv", { allowEmpty: true })
		.pipe(file_convert({from: 'csv', to: 'json'})) // 1行あたり1オブジェクトのjsonに変換
        .pipe(gulp.dest("./src/json"));

	// コールバック
    callback(); 
});


// ejs 簡単マニュアル
// ==================================================
gulp.task('ejs_manual_e', function(callback){
	var domain_url = "http://www.labelyasan.com/";

    // 改行などの加工が必要なら、markdown処理前に置換 
    // .replace(/¥¥n/g, '\n')

    // page_filename = 'index'+'.html';
    var jsonFile = require("../src/json/pages_easy.json");

    jsonFile.forEach(function (pageData, i) {	
		var page_no_00 = ('00'+pageData.page_no).slice(-2);
		var page_filename = pageData.cat_id+'_easy_'+pageData.page_id+'.html'; 
		// console.log( page_filename ); // debug

	    gulp.src("./src/ejs/template_easy.ejs")
           .pipe(process_ejs({
	           	// テンプレートファイルに渡すjsonデータ
                jsonData:{ 
					"cat_name" : pageData.cat_name,
					"cat_no"   : pageData.cat_no,
					"cat_id"   : pageData.cat_id,
					"page_name"        : pageData.page_name,
					"page_filename"    : page_filename, 
					"page_no"          : page_no_00,
					"page_id"          : pageData.page_id,
					"page_description" : compile_md(pageData.page_description),
					// "page_description_text" : pageData.page_description,

					// 可能なら後でオブジェクト化してLOOPで表示
					"block_img_00" : compile_md(pageData.block_00_img),
					"block_img_01" : compile_md(pageData.block_01_img),
					"block_img_02" : compile_md(pageData.block_02_img),
					"block_img_03" : compile_md(pageData.block_03_img),
					"block_img_04" : compile_md(pageData.block_04_img),
					"block_img_05" : compile_md(pageData.block_05_img),
					"block_img_06" : compile_md(pageData.block_06_img),
					"block_img_07" : compile_md(pageData.block_07_img),
					"block_img_08" : compile_md(pageData.block_08_img),
					"block_img_09" : compile_md(pageData.block_09_img),
					"block_img_10" : compile_md(pageData.block_10_img),
					"block_img_11" : compile_md(pageData.block_11_img),
					"block_img_12" : compile_md(pageData.block_12_img),
					"block_img_13" : compile_md(pageData.block_13_img),
					"block_img_14" : compile_md(pageData.block_14_img),
					"block_img_15" : compile_md(pageData.block_15_img),
					"block_img_16" : compile_md(pageData.block_16_img),
					"block_img_17" : compile_md(pageData.block_17_img),
					"block_img_18" : compile_md(pageData.block_18_img),
					"block_img_19" : compile_md(pageData.block_19_img),
					"block_img_20" : compile_md(pageData.block_20_img),
					"block_name_00": pageData.block_00_name,
					"block_name_01": pageData.block_01_name,
					"block_name_02": pageData.block_02_name,
					"block_name_03": pageData.block_03_name,
					"block_name_04": pageData.block_04_name,
					"block_name_05": pageData.block_05_name,
					"block_name_06": pageData.block_06_name,
					"block_name_07": pageData.block_07_name,
					"block_name_08": pageData.block_08_name,
					"block_name_09": pageData.block_09_name,
					"block_name_10": pageData.block_10_name,
					"block_name_11": pageData.block_11_name,
					"block_name_12": pageData.block_12_name,
					"block_name_13": pageData.block_13_name,
					"block_name_14": pageData.block_14_name,
					"block_name_15": pageData.block_15_name,
					"block_name_16": pageData.block_16_name,
					"block_name_17": pageData.block_17_name,
					"block_name_18": pageData.block_18_name,
					"block_name_19": pageData.block_19_name,
					"block_name_20": pageData.block_20_name,
					"block_txt_00" : compile_md(pageData.block_00_txt),
					"block_txt_01" : compile_md(pageData.block_01_txt),
					"block_txt_02" : compile_md(pageData.block_02_txt),
					"block_txt_03" : compile_md(pageData.block_03_txt),
					"block_txt_04" : compile_md(pageData.block_04_txt),
					"block_txt_05" : compile_md(pageData.block_05_txt),
					"block_txt_06" : compile_md(pageData.block_06_txt),
					"block_txt_07" : compile_md(pageData.block_07_txt),
					"block_txt_08" : compile_md(pageData.block_08_txt),
					"block_txt_09" : compile_md(pageData.block_09_txt),
					"block_txt_10" : compile_md(pageData.block_10_txt),
					"block_txt_11" : compile_md(pageData.block_11_txt),
					"block_txt_12" : compile_md(pageData.block_12_txt),
					"block_txt_13" : compile_md(pageData.block_13_txt),
					"block_txt_14" : compile_md(pageData.block_14_txt),
					"block_txt_15" : compile_md(pageData.block_15_txt),
					"block_txt_16" : compile_md(pageData.block_16_txt),
					"block_txt_17" : compile_md(pageData.block_17_txt),
					"block_txt_18" : compile_md(pageData.block_18_txt),
					"block_txt_19" : compile_md(pageData.block_19_txt),
					"block_txt_20" : compile_md(pageData.block_20_txt),

					// 可能なら後でオブジェクト化してLOOPで表示
					/*
					<% jsonData.section.forEach(function (sectionData, i) { if( sectionData.name != '' ){ %> 
					<li class=""><a href="#section_<%= i %>"><%= sectionData.name %></a></li>
					<% }}); %>
					"section":[
						{
							"section_name_X": pageData.section_X_name,
							"section_txt_X" : pageData.section_X_txt,
							"section_sub_X" : pageData.section_X_sub,
						},
					*/

					"domain_url" : domain_url
                }
            }))
		    .pipe(rename(page_filename)) // ファイル名+拡張子指定
	        .pipe(gulp.dest("../htdocs/manual_easy/")); // 出力先指定
    });



	// コールバック
    callback(); 
});


// build easy
// ==================================================
gulp.task('build_manual_e',
	gulp.series(
	'csv_pre_e', 
	'csv_to_json_e',
	'ejs_manual_e', 
	function(callback){
	// コールバック
	callback();
}));

