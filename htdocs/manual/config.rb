http_path = "/"
css_dir = "./stylesheets"
sass_dir = "./stylesheets/scss"
images_dir = "./images"
javascripts_dir = "./js"
output_style = :nested
line_comments = false