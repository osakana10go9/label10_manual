var $win = $(window),
    ua = navigator.userAgent.toLowerCase();

function initPage() {

  // objectFitImages（external js : /common/js/lib/ofi.min.js）
  objectFitImages('img.img-ofi-cover, img.img-ofi-contain');

  // position sticky（external js : /common/js/lib/sticky-state.min.js）
  // function detectSticky() {
  //   const div = document.createElement('div');
  //   div.style.position = 'sticky';
  //   return div.style.position.indexOf('sticky') !== -1;
  // }
  // if (!detectSticky()) new StickyState(document.querySelectorAll('.sticky'));

  // pagetop
  var $pt  = $('.sp-foot-nav-pagetop');
  //$pt.hide();
  $pt.find('a').on('click', function() {
    $('html, body').scrollTop(377).stop().animate({ scrollTop: 0 }, 890, 'easeOutExpo');
    return false;
  });

  // Tel link
  if ((ua.indexOf('windows') > 0 && ua.indexOf('phone') > 0) ||
      (ua.indexOf('iphone') > 0) ||
      (ua.indexOf('ipod') > 0) ||
      (ua.indexOf('android') > 0 && ua.indexOf('mobile') > 0) ||
      (ua.indexOf('firefox') > 0 && ua.indexOf('mobile') > 0) ||
      (ua.indexOf('blackberry') > 0)) {

    // 画像
    $('.tel-link img').each(function() {
      var alt = $(this).attr('alt');
      $(this).wrap($('<a>').attr('href', 'tel:' + alt.replace(/-/g, '')));
    });
  }

  // smoothLink
  $('.smooth a').smoothLink();

  // navigation
  $('#gnav > li').navigation();


  // モバイルアプリのご案内
  var $appsPopup = $('.apps-popup');
  var $closeBtn = $('.apps-popup-close');

  $('.apps-popup-btn').on('click', function(){
    $appsPopup.addClass('popup-open');
  });
  $closeBtn.on('click', function(){
    $appsPopup.removeClass('popup-open');
  });
  $('.apps-popup-overlay').on('click', function(){
    $appsPopup.removeClass('popup-open');
  })

  // Web版を開くのポップアップウィンドウ設定
  $(".popup").on('click', function() {

    var url = $(this).attr("href");
    if (! url) {
      return;
    }

    var h = 715;
    var w = 1024;
    var inH;
    var inW;
    if(window.innerWidth) {	// IE以外…IEは window.innerWidth を実装していない
      inH = window.innerHeight;
      inW = window.innerWidth;
    } else {	// IE
      inH = document.documentElement.clientHeight;
      inW = document.documentElement.clientWidth;
    }
    if ((inH >= h) && (screen.availHeight >= inH)) h = inH;
    if ((inW >= w) && (screen.availWidth >= inW)) w = inW;

    window.open(url, "labelyasan", "directories=0, location=0, menubar=0, scrollbars=0, status=0, toolbar=0, resizable=yes, scrollbars=no, width=" + w + ", height=" + h);
      return false;
  });




}

// ページ初期設定
initPage();





/**
 * deSVG
 */
deSVG('.de-svg', true);

/**
 * breakPoint
 */
$win.breakPoint({
  smartPhoneWidth: 767,
  tabletWidth: 0,
  pcMediumWidth: 0,

  onSmartPhoneEnter: function() {

    var bodyScrollTop = 0;
    var $panelBtn = $('.js-panel-btn');
    var $menuWrap = $('#gnav-wrap');

    $panelBtn.on('click', function(){

      // ナビが開いているとき
      if($panelBtn.hasClass('close')){
        $panelBtn.removeClass('close');
        $('#wrapper').css({
          display  : ' block',
          overflow : 'auto'
        });
        $menuWrap.hide().removeClass('show');
        $('body').removeClass('gnav-open');
        $('html,body').scrollTop(bodyScrollTop);
        return false;

      }else{

        bodyScrollTop = $(window).scrollTop();

        $panelBtn.addClass('close');
        $('#wrapper').css({
          display  : 'none',
          overflow : 'hidden'
        });
        $menuWrap.show().addClass('show');
        $('body').addClass('gnav-open');

        return false;
      }
    });

    // gnavのアコーディオン1
    $('.has-child span').on('click', function(){
      $(this).parent().next('.gnav-child-wrap').slideToggle();
      $(this).toggleClass('gnav-child-open');
      return false;
    })

    // アンカー固定ナビ
    var $spFixedAnchor = $('.sp-anchor-nav');
    var $spAnchorNav = $('.sp-anchor-nav-select');
    var $spAnchorNavItem = $('.sp-anchor-nav-select li');
    var $spAnchorCurrent = $('.sp-anchor-nav-current');
    var $spHeaderH = $('#head').outerHeight(true);

    $win.on('load scroll', function() {
      if ($win.scrollTop() > $spHeaderH)
        $spFixedAnchor.addClass('is-fixed');
      else
        $spFixedAnchor.removeClass('is-fixed');
    });

    // タイトルクリックでアンカーリスト表示
    $spAnchorCurrent.on('click',function(){
      $spAnchorNav.slideToggle(300);
    })

    // アンカーリスト選択後
    $spAnchorNavItem.on('click', function(){
      var $spAnchorNavText = $(this).text();
      var href = $(this).find('a').attr('href');

      $('body,html').animate({
          scrollTop: $(href).offset().top - 20
      }, 800, 'swing');

      $spAnchorCurrent.text($spAnchorNavText);
      $spAnchorNav.css('display', 'none');

      if($(this).hasClass('current')){

      } else {
        $spAnchorNavItem.removeClass('current');
        $(this).addClass('current');
      }

      return false;

    });



    // フッター固定ナビ
    var $spFixedNavi  = $('.sp-foot-nav');

    $win.on('load scroll', function() {
      if ($win.scrollTop() > 200)
        $spFixedNavi.addClass('is-fixed');
      else
        $spFixedNavi.removeClass('is-fixed');
    });
  },
  onPcEnter: function() {
    $('.has-child span').off('click');

    //該当のセレクタなどを代入
    var mainArea = $('.main-cont'); //メインコンテンツ
    var sideWrap = $('.side'); //サイドバーの外枠
    var sideArea = $('.side-nav'); //サイドバー

    var wd = $(window); //ウィンドウ自体

    //メインとサイドの高さを比べる
    var mainH = mainArea.outerHeight(true);
    var sideH = sideWrap.height();


    if(sideH < mainH) { //メインの方が高ければ色々処理する

      //サイドバーの外枠をメインと同じ高さにしてrelaltiveに（#sideをポジションで上や下に固定するため）
      sideWrap.css({'height': mainH,'position': 'relative'});

      //サイドバーがウィンドウよりいくらはみ出してるか
      var sideOver = wd.height()-sideArea.height();

      //固定を開始する位置 = サイドバーの座標＋はみ出す距離
      //var starPoint = sideArea.offset().top + (-sideOver);
      var starPoint = sideArea.offset().top;

      //固定を解除する位置 = メインコンテンツの終点
      var breakPoint = sideArea.offset().top + mainH;

      wd.scroll(function() { //スクロール中の処理

        if(wd.height() < sideArea.height()){ //サイドメニューが画面より大きい場合
          if(starPoint < wd.scrollTop() && wd.scrollTop() + wd.height() < breakPoint){ //固定範囲内
            sideArea.css({'position': 'fixed', 'bottom': '20px'});

          }else if(wd.scrollTop() + wd.height() >= breakPoint){ //固定解除位置を超えた時
            sideArea.css({'position': 'absolute', 'bottom': '0'});

          } else { //その他、上に戻った時
            sideArea.css('position', 'static');

          }

        }else{ //サイドメニューが画面より小さい場合

          var sideBtm = wd.scrollTop() + sideArea.height(); //サイドメニューの終点

          if(mainArea.offset().top < wd.scrollTop() && sideBtm < breakPoint){ //固定範囲内
            sideArea.css({'position': 'fixed', 'top': '0'});

          }else if(sideBtm >= breakPoint){ //固定解除位置を超えた時

            //サイドバー固定場所（bottom指定すると不具合が出るのでtopからの固定位置を算出する）
            var fixedSide = mainH - sideH;

            sideArea.css({'position': 'absolute', 'top': fixedSide});

          } else {
            sideArea.css('position', 'static');
          }
        }


      });

    }

    // サイドナビの現在地設定（アンカーリンク）
    // ナビゲーションのリンクを指定
    var navLink = $('.side-anchor li a'),
        footerHeight = $('footer').outerHeight();

    // 各コンテンツのページ上部からの開始位置と終了位置を配列に格納しておく
    var contentsArr = new Array();
    for (var i = 0; i < navLink.length; i++) {
       // コンテンツのIDを取得
      var targetContents = navLink.eq(i).attr('href');
      // ページ内リンクでないナビゲーションが含まれている場合は除外する
      if(targetContents.charAt(0) == '#') {
         // ページ上部からコンテンツの開始位置までの距離を取得
            var targetContentsTop = $(targetContents).offset().top;
         // ページ上部からコンテンツの終了位置までの距離を取得
            var targetContentsBottom = targetContentsTop + $(targetContents).outerHeight(true) - 1;
         // 配列に格納
            contentsArr[i] = [targetContentsTop, targetContentsBottom]
      }
    };

    // 現在地をチェックする
    function currentCheck() {
       // 現在のスクロール位置を取得
        var windowScrolltop = $(window).scrollTop();
        for (var i = 0; i < contentsArr.length; i++) {
           // 現在のスクロール位置が、配列に格納した開始位置と終了位置の間にあるものを調べる
          if(contentsArr[i][0] <= windowScrolltop && contentsArr[i][1] >= windowScrolltop) {
                // 開始位置と終了位置の間にある場合、ナビゲーションにclass="current"をつける
               navLink.removeClass('current');
               navLink.eq(i).addClass('current');
                i == contentsArr.length;
            }

       };
    }

    // ページ読み込み時とスクロール時に、現在地をチェックする
    $(window).on('load scroll', function() {
      currentCheck();
    });

    // $win.on('load scroll', function() {
    //   if ($win.scrollTop() > 100) $pt.fadeIn(); else $pt.fadeOut();
    // });

    // ナビゲーションをクリックした時のスムーズスクロール
    // navLink.click(function() {
    //   $('html,body').animate({
    //       scrollTop: $($(this).attr('href')).offset().top
    //    }, 300);
    //     return false;
    // })


  },
  onSmartPhoneLeave: function() {
    // スマホモード解除時の処理を書く
  },
  onPcLeave: function() {
    // PCモード解除時の処理を書く
  }
});
