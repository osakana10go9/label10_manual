
$('.sp-anchor-nav').change(function () {
    var speed = 800;
    var href = $(this).val();
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('body,html').animate({
        scrollTop: position
    }, speed, 'swing');
    return false;
});


/*
$('#nav__page').change(function () {
    var speed = 800;
    var href = $(this).val();
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('body,html').animate({
        scrollTop: position
    }, speed, 'swing');
    return false;
});
*/