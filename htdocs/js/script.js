'use strict';

var $win = $(window),
    resizeTimer = function resizeTimer(func, interval) {
  var _timer = null;
  window.onresize = function () {
    clearTimeout(_timer);
    _timer = setTimeout(function () {
      func();
    }, interval);
  };
};

$.fn.animateTransform = function (properties, duration, ease, callback) {
  ease = ease || 'ease';
  var _timer = null;
  var $this = this;
  var cssOrig = {
    transition: $this.css('transition')
  };
  $this.clearQueue();
  return $this.queue(function (next) {
    properties['transition'] = 'transform ' + duration + 'ms ' + ease;
    $this.css(properties);
    clearTimeout(_timer);
    _timer = setTimeout(function () {
      $this.css(cssOrig);
      next();
      callback();
    }, duration);
  });
};

function setTopSceneSlider() {

  var $sliderWrap = $('#top-scene-slider-wrap'),
      $slider = $('#top-scene-slider'),
      $sliderCln = $slider.find('.top-scene-slide').clone(),
      $sliderCln2 = $sliderCln.clone(),
      $slidesCnt = $slider.find('.top-scene-slide').length;

  var $slides = null,
      $currentSlide = null,
      moveX = 0,
      // 移動量
  count = 0,
      speed = window.innerWidth > 767 ? 8000 : 6000,
      activeClass = '-active',
      activeSlides = [];

  // スライドクローンを追加
  $slider.append($sliderCln).append($sliderCln2);
  $slides = $slider.find('.top-scene-slide');

  var startSlider = function startSlider() {

    // reset
    if (count == $slidesCnt * 2) {
      $slider.find('.top-scene-slide.' + activeClass).removeClass(activeClass);
      $slider.css('transform', 'translate3d(0,0,0)');
      moveX = 0;
      count = 0;
    }

    moveX += window.innerWidth > 767 ? $sliderWrap.outerWidth() / 3 : $sliderWrap.outerWidth();
    count++;

    // カレントスライドを取得
    if (window.innerWidth > 767) {
      $currentSlide = $slides.eq(count + 1);
      activeSlides.push($currentSlide);

      if (activeSlides.length > 3) {
        activeSlides[0].removeClass(activeClass);
        activeSlides.shift();
      }
    } else {
      $currentSlide = $slides.eq(count);
      activeSlides.push($currentSlide);

      if (activeSlides.length > 2) {
        activeSlides[0].removeClass(activeClass);
        activeSlides.shift();
      }
    }

    // カレントスライドをアクティブに設定
    $currentSlide.addClass(activeClass);

    // 移動
    $slider.stop(true).animateTransform({
      'transform': 'translate3d(-' + moveX + 'px,0,0)'
    }, speed, 'linear', startSlider);
  };

  var resetSlider = function resetSlider() {
    var slideW = window.innerWidth > 767 ? $sliderWrap.outerWidth() / 3 : $sliderWrap.outerWidth();
    $currentSlide = null;
    activeSlides = [];
    moveX = 0;
    count = 0;

    $slider.find('.top-scene-slide.' + activeClass).removeClass(activeClass);
    $slides.css('width', slideW);
    $slider.attr('style', '');
    $slider.css('width', slideW * $slides.length + 'px');
    $slider.stop('');
    startSlider();
  };

  resetSlider();
  // resizeTimer(resetSlider, 100);
}

function setTopItemSlider() {

  var $slideCont = $('.top-item-slider .slide-cont');
  var maxH = 0;

  $slideCont.each(function (i, el) {
    var h = $(this).height();
    if (maxH < h) maxH = h;
  });

  $slideCont.css('height', maxH);

  $('.top-item-slider').on('init', function (slick) {
    var $this = $(this);
    var arrowsTop = ($this.outerHeight() - $this.find('.slide-cat').outerHeight()) / 2 - $this.find('.slick-prev').outerHeight() / 2;
    $this.find('.slick-prev, .slick-next').css('top', arrowsTop);
  }).slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
}

function setTopBannerSlider() {

  $('.top-banner-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
}

var tag = document.createElement('script');
var firstScriptTag = document.getElementsByTagName('script')[0];
tag.src = "https://www.youtube.com/iframe_api";
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var ytPlayer = void 0;

function onYouTubeIframeAPIReady() {
  ytPlayer = new YT.Player('yt', {
    width: 800,
    height: 450,
    videoId: 'fdg9whnIbEg',
    playerVars: {
      rel: 0,
      showinfo: 0
    },
    events: {
      'onReady': onPlayerReady
    }
  });
}

function setMainVisualMovie() {
  var $moviePopup = $('#movie-popup'),
      $movieThumb = $('#movie-thumb'),
      visibleClass = '-visible';

  $movieThumb.on('click', function () {
    $moviePopup.addClass(visibleClass);
    setTimeout(function () {
      ytPlayer.playVideo();
    }, 400);
  });

  $moviePopup.on('click', function () {
    $moviePopup.removeClass(visibleClass);
    ytPlayer.seekTo(0, true);
    ytPlayer.pauseVideo();
  });
}

function onPlayerReady(event) {
  setMainVisualMovie();
}

// Internet Explorerでご利用時の注意点
function setIePopup() {
  var $iePopup = $('.main-visual-ie-popup');
  var $closeBtn = $('.ie-popup-close');

  $('.main-visual-ie').on('click', function () {
    $iePopup.addClass('popup-open');
  });
  $closeBtn.on('click', function () {
    $iePopup.removeClass('popup-open');
  });
  $('.ie-popup-overlay').on('click', function () {
    $iePopup.removeClass('popup-open');
  });
}

// モバイルアプリのご案内
function setAppsPopup() {
  var $appsPopup = $('.main-visual-apps-popup');
  var $closeBtn = $('.apps-popup-close');

  $('.main-visual-btn').on('click', function () {
    $appsPopup.addClass('popup-open');
  });
  $closeBtn.on('click', function () {
    $appsPopup.removeClass('popup-open');
  });
  $('.apps-popup-overlay').on('click', function () {
    $appsPopup.removeClass('popup-open');
  });
}

// 活用シーンスライダー
setTopSceneSlider();

// 用紙選びスライダー
setTopItemSlider();

// バナースライダー
setTopBannerSlider();

// Internet Explorerでご利用時の注意点
setIePopup();

// モバイルアプリのご案内
setAppsPopup();

// FEATURE in-view
inView(".top-feature-sct").on("enter", function (el) {

  $(el).addClass('is-enter');

  var $featureImg = $(el).find(".box-img img");
  $featureImg.each(function (i, el) {
    var $inImg = $(el);
    var dataSrc = $(el).attr("data-src");
    $(el).attr("src", dataSrc).removeAttr("data-src");
  });
});

// 高さ揃え
$('.slide .slide-overlay').matchHeight();

/**
 * breakPoint
 */
$win.breakPoint({
  smartPhoneWidth: 767,
  tabletWidth: 0,
  pcMediumWidth: 0,

  onSmartPhoneEnter: function onSmartPhoneEnter() {},
  onPcEnter: function onPcEnter() {
    inView.offset(400);
  },
  onSmartPhoneLeave: function onSmartPhoneLeave() {},
  onPcLeave: function onPcLeave() {}
});
